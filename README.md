# Rock Paper Scissors

Rock Paper Scissors is a project that simulate a rock - paper - scissors game. There are a player versus bot mode and a bot versus bot mode. This project is a test given to me by a company.

## Prerequisites

Install [Android Studio](https://developer.android.com/studio) to open Rock Paper Scissors.

## Used technologies

The project is writing in Kotlin. There is a [navigation graph](https://developer.android.com/guide/navigation/navigation-getting-started) too for navigation between fragment.

## Tests
Tests are here for testing the game motor. They tests the generation of movement by computer and the different cases of victories, failures or equals possible during a game.


## How to add movements ?
For add a movement, you have to add an enum case in Movement class, in model directory. You must add a string res too for the translatable name of movement.