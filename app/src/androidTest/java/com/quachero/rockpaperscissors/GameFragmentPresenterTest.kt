package com.quachero.rockpaperscissors

import com.quachero.rockpaperscissors.model.Computer
import com.quachero.rockpaperscissors.model.Movement
import com.quachero.rockpaperscissors.model.Player
import com.quachero.rockpaperscissors.presenter.GameFragmentPresenter
import org.junit.Test

import org.junit.Assert.*

/**
 * Game fragment presenter test
 *
 * @author Alaric Quachero
 *
 * @created 25/05/2019
 *
 * @last_update 25/05/2019 by Alaric Quachero
 */
class GameFragmentPresenterTest {
    private val gameFragmentPresenter: GameFragmentPresenter = GameFragmentPresenter()

    /**
     * Test of computerMakeAMovement function (good if result of the function is an existing Movement)
     */
    @Test
    fun computerMakeAMovementTest() {
        // Arrange
        val computer = Computer()

        // Act
        val isAExistingMovement = Movement.values().contains(gameFragmentPresenter.computerMakeAMovement(computer))

        // Assert
        assertEquals(true, isAExistingMovement)
    }

    /**
     * Test of getWinnerOfTheGame function (good if result of the function is null (equality)
     */
    @Test
    fun getWinnerOfTheGameRockVsRockTest() {
        // Arrange
        val player1 = Player()
        val player2 = Player()

        // Act
        val winner = gameFragmentPresenter.getWinnerOfTheGame(Pair(player1, Movement.ROCK), Pair(player2, Movement.ROCK))

        // Assert
        assertEquals(null, winner)
    }

    /**
     * Test of getWinnerOfTheGame function (good if result of the function is player2 (rock lose against paper)
     */
    @Test
    fun getWinnerOfTheGameRockVsPaperTest() {
        // Arrange
        val player1 = Player()
        val player2 = Player()

        // Act
        val winner = gameFragmentPresenter.getWinnerOfTheGame(Pair(player1, Movement.ROCK), Pair(player2, Movement.PAPER))

        // Assert
        assertEquals(player2, winner)
    }

    /**
     * Test of getWinnerOfTheGame function (good if result of the function is player1 (rock beats scissors)
     */
    @Test
    fun getWinnerOfTheGameRockVsScissorsTest() {
        // Arrange
        val player1 = Player()
        val player2 = Player()

        // Act
        val winner = gameFragmentPresenter.getWinnerOfTheGame(Pair(player1, Movement.ROCK), Pair(player2, Movement.SCISSORS))

        // Assert
        assertEquals(player1, winner)
    }

    /**
     * Test of getWinnerOfTheGame function (good if result of the function is player1 (paper beats rock)
     */
    @Test
    fun getWinnerOfTheGamePaperVsRockTest() {
        // Arrange
        val player1 = Player()
        val player2 = Player()

        // Act
        val winner = gameFragmentPresenter.getWinnerOfTheGame(Pair(player1, Movement.PAPER), Pair(player2, Movement.ROCK))

        // Assert
        assertEquals(player1, winner)
    }

    /**
     * Test of getWinnerOfTheGame function (good if result of the function is null (equality)
     */
    @Test
    fun getWinnerOfTheGamePaperVsPaperTest() {
        // Arrange
        val player1 = Player()
        val player2 = Player()

        // Act
        val winner = gameFragmentPresenter.getWinnerOfTheGame(Pair(player1, Movement.PAPER), Pair(player2, Movement.PAPER))

        // Assert
        assertEquals(null, winner)
    }

    /**
     * Test of getWinnerOfTheGame function (good if result of the function is player2 (paper lose against scissors)
     */
    @Test
    fun getWinnerOfTheGamePaperVsScissorsTest() {
        // Arrange
        val player1 = Player()
        val player2 = Player()

        // Act
        val winner = gameFragmentPresenter.getWinnerOfTheGame(Pair(player1, Movement.PAPER), Pair(player2, Movement.SCISSORS))

        // Assert
        assertEquals(player2, winner)
    }

    /**
     * Test of getWinnerOfTheGame function (good if result of the function is player2 (scissors lose against rock)
     */
    @Test
    fun getWinnerOfTheGameScissorsVsRockTest() {
        // Arrange
        val player1 = Player()
        val player2 = Player()

        // Act
        val winner = gameFragmentPresenter.getWinnerOfTheGame(Pair(player1, Movement.SCISSORS), Pair(player2, Movement.ROCK))

        // Assert
        assertEquals(player2, winner)
    }

    /**
     * Test of getWinnerOfTheGame function (good if result of the function is player1 (scissors beats paper)
     */
    @Test
    fun getWinnerOfTheGameScissorsVsPaperTest() {
        // Arrange
        val player1 = Player()
        val player2 = Player()

        // Act
        val winner = gameFragmentPresenter.getWinnerOfTheGame(Pair(player1, Movement.SCISSORS), Pair(player2, Movement.PAPER))

        // Assert
        assertEquals(player1, winner)
    }

    /**
     * Test of getWinnerOfTheGame function (good if result of the function is null (equality)
     */
    @Test
    fun getWinnerOfTheGameScissorsVsScissorsTest() {
        // Arrange
        val player1 = Player()
        val player2 = Player()

        // Act
        val winner = gameFragmentPresenter.getWinnerOfTheGame(Pair(player1, Movement.SCISSORS), Pair(player2, Movement.SCISSORS))

        // Assert
        assertEquals(null, winner)
    }
}