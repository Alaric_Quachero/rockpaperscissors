package com.quachero.rockpaperscissors.view.viewInterface

/**
 * Select mode fragment view interface
 *
 * @author Alaric Quachero
 *
 * @created 25/05/2019
 *
 * @last_update 25/05/2019 by Alaric Quachero
 */
interface SelectModeFragmentViewInterface {}