package com.quachero.rockpaperscissors.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.quachero.rockpaperscissors.R
import com.quachero.rockpaperscissors.model.Movement
import com.quachero.rockpaperscissors.view.adapter.MovementAdapter
import com.quachero.rockpaperscissors.view.viewInterface.GameFragmentViewInterface
import kotlinx.android.synthetic.main.fragment_select_movement.view.*

/**
 * Select movement fragment view
 *
 * @author Alaric Quachero
 *
 * @created 25/05/2019
 *
 * @last_update 25/05/2019 by Alaric Quachero
 */
class SelectMovementFragmentView : Fragment(), GameFragmentViewInterface {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_select_movement, container, false)

        view.rvSelectMovementMovementsListing.layoutManager = LinearLayoutManager(requireContext())
        view.rvSelectMovementMovementsListing.adapter = MovementAdapter(requireContext(), Movement.values())

        return view
    }
}