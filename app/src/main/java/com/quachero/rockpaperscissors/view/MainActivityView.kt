package com.quachero.rockpaperscissors.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.quachero.rockpaperscissors.R

/**
 * Main activity view
 *
 * @author Alaric Quachero
 *
 * @created 25/05/2019
 *
 * @last_update 25/05/2019 by Alaric Quachero
 */
class MainActivityView : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
