package com.quachero.rockpaperscissors.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.quachero.rockpaperscissors.R
import com.quachero.rockpaperscissors.model.Computer
import com.quachero.rockpaperscissors.model.Movement
import com.quachero.rockpaperscissors.model.Player
import com.quachero.rockpaperscissors.model.User
import com.quachero.rockpaperscissors.presenter.GameFragmentPresenter
import com.quachero.rockpaperscissors.view.viewInterface.GameFragmentViewInterface
import kotlinx.android.synthetic.main.fragment_game.view.*

/**
 * Game fragment view
 *
 * @author Alaric Quachero
 *
 * @created 25/05/2019
 *
 * @last_update 11/06/2019 by Alaric Quachero
 */
class GameFragmentView : Fragment(), GameFragmentViewInterface {

    private var gameFragmentPresenter: GameFragmentPresenter? = null
    private var player1: Player? = null
    private var player2: Player? = null

    private var movementName: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_game, container, false)

        gameFragmentPresenter = GameFragmentPresenter()

        movementName = arguments?.getString("movementName", null)
        val user = arguments?.getParcelable<User>("user")

        if(user != null){
            player1 = user
            player2 = Computer("Computer 1")
        }
        else{
            player1 = Computer("Computer 1")
            player2 = Computer("Computer 2")
        }

        view.tvFragmentGamePlayer1Name.text = player1?.name
        view.tvFragmentGamePlayer2Name.text = player2?.name

        startMatch(view)

        view.buttonStartGame.setOnClickListener {
            if(user != null){
                findNavController().popBackStack()
            } else {
                startMatch(view)
            }
        }

        return view
    }

    fun startMatch(view: View){
        val movementPlayer1: Movement? = movementName?.let {
            Movement.valueOf(it)

        } ?: run {
            (player1 as? Computer)?.makeAMovement()
        }
        val movementPlayer2: Movement? = (player2 as? Computer)?.makeAMovement()
        var winner: Player? = null

        if(movementPlayer1 != null && movementPlayer2 != null){
            winner = gameFragmentPresenter?.getWinnerOfTheGame(Pair(player1 ?: User(), movementPlayer1), Pair(player2 ?: User(), movementPlayer2))
        }

        view.tvFragmentGamePlayer1Movement.text = getString(movementPlayer1?.nameRes ?: 0)
        view.tvFragmentGamePlayer2Movement.text = getString(movementPlayer2?.nameRes ?: 0)

        if(winner != null){
            view.tvFragmentGameResultGame.text = getString(R.string.fragment_game_x_win, winner.name)
        }
        else{
            view.tvFragmentGameResultGame.text = getString(R.string.fragment_game_equality)
        }
    }
}