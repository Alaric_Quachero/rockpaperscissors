package com.quachero.rockpaperscissors.view.adapter

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation.findNavController
import com.quachero.rockpaperscissors.R
import com.quachero.rockpaperscissors.model.Movement
import com.quachero.rockpaperscissors.model.User
import kotlinx.android.synthetic.main.row_movement.view.*

/**
 * Movement adapter
 *
 * @author Alaric Quachero
 *
 * @created 25/05/2019
 *
 * @last_update 25/05/2019 by Alaric Quachero
 */
class MovementAdapter(val mContext: Context, val movementsList: Array<Movement>) :
    RecyclerView.Adapter<MovementAdapter.MyViewHolder>() {

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val buttonRowMovementMovementName = view.buttonRowMovementMovementName
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): MyViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.row_movement, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int = movementsList.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val movement = movementsList[position]

        holder.buttonRowMovementMovementName.text = mContext.getString(movement.nameRes)
        holder.buttonRowMovementMovementName.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("movementName", movement.name)
            bundle.putParcelable("user", User("Player 1"))
            findNavController(holder.itemView).navigate(R.id.action_selectMovementFragmentView_to_gameFragmentView, bundle)

        }
    }
}

