package com.quachero.rockpaperscissors.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.quachero.rockpaperscissors.R
import com.quachero.rockpaperscissors.view.viewInterface.GameFragmentViewInterface
import kotlinx.android.synthetic.main.fragment_select_mode.view.*

/**
 * Select mode fragment view
 *
 * @author Alaric Quachero
 *
 * @created 25/05/2019
 *
 * @last_update 25/05/2019 by Alaric Quachero
 */
class SelectModeFragmentView : Fragment(), GameFragmentViewInterface {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_select_mode, container, false)

        view.buttonSelectModeComputerVersusComputer.setOnClickListener {
            findNavController().navigate(R.id.action_selectModeFragmentView_to_gameFragmentView)
        }

        view.buttonSelectModeUserVersusComputer.setOnClickListener {
            findNavController().navigate(R.id.action_selectModeFragmentView_to_selectMovementFragmentView)
        }

        return view
    }
}