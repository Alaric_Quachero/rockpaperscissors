package com.quachero.rockpaperscissors.model

/**
 * Computer object
 *
 * @author Alaric Quachero
 *
 * @created 25/05/2019
 *
 * @last_update 25/05/2019 by Alaric Quachero
 */
class Computer() : Player() {

    /**
     * Constructor
     *
     * @param name The name of the computer
     */
    constructor(name: String) : this() {
        this.name = name
    }

    /**
     * Make a movement
     *
     * @return The movement of the computer
     */
    fun makeAMovement(): Movement{
        return makeARandomMovement()
    }
}