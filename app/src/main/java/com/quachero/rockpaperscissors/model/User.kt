package com.quachero.rockpaperscissors.model

import android.os.Parcelable
import android.os.Parcel

/**
 * User object
 *
 * @author Alaric Quachero
 *
 * @created 25/05/2019
 *
 * @last_update 25/05/2019 by Alaric Quachero
 */
class User() : Player(), Parcelable {

    /**
     * Constructor
     *
     * @param name The name of the computer
     */
    constructor(name: String) : this() {
        this.name = name
    }

    constructor(source: Parcel) : this() {
        name = source.readString()
    }

    // **************************************************
    // Parcelable
    // **************************************************
    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(name)
    }

    companion object CREATOR : Parcelable.Creator<User> {
        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }

        override fun createFromParcel(source: Parcel): User {
            return User(source)
        }
    }
}