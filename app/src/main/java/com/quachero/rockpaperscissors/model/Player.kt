package com.quachero.rockpaperscissors.model

/**
 * Player interface
 *
 * @author Alaric Quachero
 *
 * @created 25/05/2019
 *
 * @last_update 25/05/2019 by Alaric Quachero
 */
open class Player() {
    var name: String? = null

    /**
     * Constructor
     *
     * @param name The name of the player
     */
    constructor(name: String) : this() {
        this.name = name
    }

    /**
     * Make a random movement
     *
     * @return A random movement
     */
    fun makeARandomMovement(): Movement{
        return Movement.random()
    }
}