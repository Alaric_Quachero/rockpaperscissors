package com.quachero.rockpaperscissors.model

import com.quachero.rockpaperscissors.R

/**
 * Movement enum
 *
 * @author Alaric Quachero
 *
 * @created 25/05/2019
 *
 * @last_update 25/05/2019 by Alaric Quachero
 */
enum class Movement(val nameRes: Int, val beatsList: Lazy<List<Movement>>) {
    ROCK(R.string.movement_name_rock, lazy { listOf(SCISSORS) }),
    PAPER(R.string.movement_name_paper, lazy { listOf(ROCK) }),
    SCISSORS(R.string.movement_name_scissors, lazy { listOf(PAPER) });

    companion object {

        /**
         * Generate a random movement
         *
         * @return A random movement
         */
        fun random(): Movement = values().random()
    }
}

/**
 * check if movement beats another movement
 *
 * @param movement The other movement
 *
 * @return true if the movement beats the other movement
 */
fun Movement.beats(movement: Movement?): Boolean = this.beatsList.value.contains(movement)