package com.quachero.rockpaperscissors.presenter.presenterInterface

import com.quachero.rockpaperscissors.model.Computer
import com.quachero.rockpaperscissors.model.Movement
import com.quachero.rockpaperscissors.model.Player


/**
 * Game fragment presenter interface
 *
 * @author Alaric Quachero
 *
 * @created 25/05/2019
 *
 * @last_update 25/05/2019 by Alaric Quachero
 */
interface GameFragmentPresenterInterface {

    /**
     * Computer make a movement
     *
     * @param computer The computer which make the movement
     *
     * @return The movement of the computer
     */
    fun computerMakeAMovement(computer: Computer): Movement?

    /**
     * Get the winner of the game
     *
     * @param firstPlayer A pair of player and his movement
     *
     * @param secondPlayer A pair of player and his movement
     *
     * @return The player who win the game
     */
    fun getWinnerOfTheGame(firstPlayer: Pair<Player, Movement>, secondPlayer: Pair<Player, Movement>): Player?
}