package com.quachero.rockpaperscissors.presenter

import com.quachero.rockpaperscissors.model.Computer
import com.quachero.rockpaperscissors.model.Movement
import com.quachero.rockpaperscissors.model.Player
import com.quachero.rockpaperscissors.model.beats
import com.quachero.rockpaperscissors.presenter.presenterInterface.GameFragmentPresenterInterface

/**
 * Game fragment presenter
 *
 * @author Alaric Quachero
 *
 * @created 25/05/2019
 *
 * @last_update 25/05/2019 by Alaric Quachero
 */
class GameFragmentPresenter : GameFragmentPresenterInterface {

    /**
     * Implementation of computer make a movement
     *
     * @param computer The computer which make the movement
     *
     * @return The movement of the computer
     */
    override fun computerMakeAMovement(computer: Computer): Movement? {
        return computer.makeAMovement()
    }

    /**
     * Implementation of getWinnerOfTheGame
     *
     * @param firstPlayer A pair of player and his movement
     *
     * @param secondPlayer A pair of player and his movement
     *
     * @return The player who win the game
     */
    override fun getWinnerOfTheGame(firstPlayer: Pair<Player, Movement>, secondPlayer: Pair<Player, Movement>): Player?{
        return when {
            firstPlayer.second == secondPlayer.second -> null
            firstPlayer.second.beats(secondPlayer.second) -> return firstPlayer.first
            else -> return secondPlayer.first
        }
    }
}